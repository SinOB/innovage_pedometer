<?php
/*
 * Allows a group admin to manage the partnerships. This is accessable from the 
 * group menu under the tab heading 'Partners'. 
 */

/** /
 * Add a new menu option for partner management to the Groups sub menu
 * 
 * @global type $bp
 */
function innovage_pedometer_partner_admin_nav() {
    global $bp;

    if (!is_user_logged_in()) {
        return;
    }

    //only add if user is an admin or if user is a group admin or is editor
    if (!(bp_group_is_admin() || current_user_can('edit_pages'))) {
        return;
    }

    if (!isset($bp->groups->current_group)) {
        return;
    }

    if (!bp_is_groups_component()) {
        return;
    }

    $group = $bp->groups->current_group;

    if (!isset($group) || !isset($group->id)) {
        return;
    }

    // If challenge is not collaborative/competitive dyad type return
    $challenge_type = groups_get_groupmeta($group->id, 'challenge-approach');
    if (!($challenge_type == 1 || $challenge_type == 2)) {
        return;
    }

    if (isset($bp->groups->current_group->slug)) {
        bp_core_new_subnav_item(array(
            'name' => 'Partners',
            'slug' => 'partners',
            'parent_slug' => $bp->groups->current_group->slug,
            'parent_url' => bp_get_group_permalink($bp->groups->current_group),
            'screen_function' => 'innovage_pedometer_partner_show_admin_screen',
            'position' => 42));
    }
}

add_action('bp_setup_nav', 'innovage_pedometer_partner_admin_nav');

/** /
 * Call functions to handle content and templating
 */
function innovage_pedometer_partner_show_admin_screen() {

    add_action('bp_template_content', 'innovage_pedometer_partner_show_admin_screen_content');

    $templates = array('groups/single/plugins.php', 'plugin-template.php');
    if (strstr(locate_template($templates), 'groups/single/plugins.php')) {
        bp_core_load_template(apply_filters('bp_core_template_plugin', 'groups/single/plugins'));
    } else {
        bp_core_load_template(apply_filters('bp_core_template_plugin', 'plugin-template'));
    }
}

/** /
 * Handle the content to be dispayed
 *
 * @global type $bp
 * @return type
 */
function innovage_pedometer_partner_show_admin_screen_content() {
    global $bp;

    $group_id = $bp->groups->current_group->id;

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST["innopt_partner_action"]) &&
                $_POST["innopt_partner_action"] === 'delete') {
            $team_id = intval($_POST["innopt_team_id"]);
            innovage_partnership_delete($team_id);
        }

        if (isset($_POST["innopt_admin_action"]) &&
                $_POST["innopt_admin_action"] === 'admin_create') {
            innovage_pedometer_create_admin_partners($group_id);
        }
    }


    $results = innovage_partner_get_partners_by_group($group_id);
    innovage_pedometer_show_admin_create_partner_form($group_id);
    innovage_pedometer_display_partners($results);
}

/** /
 * Dispaly a list of all of the partnerships within the group
 * 
 * @param type $partners
 */
function innovage_pedometer_display_partners($partners) {

    echo '<h3>Existing group partnerships</h3>';

    if (!isset($partners) || empty($partners) || count($partners) == 0) {
        echo "<p>There are currently no partners set up in this group.</p>";
        return;
    }
    ?>
    <p>Each row represents a partnership.</p>
    <form method="post" id="innopt_make_partnership">
        <input type="hidden" name = "innopt_team_id" id="innopt_team_id" value="">
        <input type="hidden" name="innopt_partner_action" id="innopt_partner_action" value="">
    </form>
    <table>
        <?php
        foreach ($partners as $team_id => $partners) {
            $partner_0 = get_userdata($partners[0]);
            $partner_1 = get_userdata($partners[1]);
            echo "<tr><td>" . $partner_0->display_name . "</td>"
            . "<td>" . $partner_1->display_name . "</td>"
            . "<td><a href='javascript:submitInnopdDeletePartnershipForm(" . $team_id . ")'>Unpartner</a></td>"
            . "</tr>";
        }
        ?>
    </table><?php
}

/** /
 * Dispaly the form to create a partnership between two unpartnered members
 * of a group
 * 
 * @global type $bp
 * @param type $group_id
 */
function innovage_pedometer_show_admin_create_partner_form($group_id) {
    global $bp;
    $partners = array();
    $partners[''] = '';

    // get all the members in the group
    $members = BP_Groups_Member::get_all_for_group($group_id, false, false, false);
    foreach ($members['members'] as $member) {
        if (!innovage_partner_user_has_group_partner($group_id, $member->user_id)) {
            $partners[$member->user_id] = $member->user_login . ' (' . $member->display_name . ')';
        }
    }
    ?>
    <form method="post" id="innopt_create_admin_partnership">
        <h3>Create a partnership</h3>

        <p>Choose the two people you would like to partner with one another in 
            this group. It is not possible to create a partnership with a 
            person who already has a partner in the group.</p>
        Partner the member
        <select name="partner_1">
            <?php
            foreach ($partners as $id => $name) {
                echo '<option value="' . $id . '">' . $name . '</option>';
            }
            ?>
        </select>
        with the member
        <select name="partner_2">
            <?php
            foreach ($partners as $id => $name) {
                echo '<option value="' . $id . '">' . $name . '</option>';
            }
            ?>
        </select>
        <br/><br/>
        <input type="hidden" name="group_id" value="<?php echo $group_id ?>">
        <input type="hidden" name="innopt_admin_action" id="innopt_admin_action" value="admin_create">
        <input type="submit" value="Create Partnership">
    </form>
    <?php
}

/** /
 * Save the specified partners from the admin form
 *
 * @param type $group_id
 */
function innovage_pedometer_create_admin_partners($group_id) {
    $partner1 = intval($_POST["partner_1"]);
    $partner2 = intval($_POST["partner_2"]);

    $errors = innovage_partnership_save_both($partner1, $partner2, $group_id);
    innovage_pedometer_printErrorMessages($errors);
}
