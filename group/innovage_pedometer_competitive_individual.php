<?php

/** /
 * Add Individual Steps Tab for competitive challenge to the groups page.
 * @global type $bp
 */
function innovage_pedometer_add_competitive_individual_steps_page() {
    global $bp;

    if (!isset($bp->groups->current_group)) {
        return;
    }

    if (!bp_is_groups_component()) {
        return;
    }

    $group = $bp->groups->current_group;

    if (!isset($group) || !isset($group->id)) {
        return;
    }

    // If challenge is not collaborative dyad type return
    $challenge_type = groups_get_groupmeta($group->id, 'challenge-approach');
    if (!($challenge_type == 1 || $challenge_type == 3)) {
        return;
    }

    // Only display this tab once the user is a member of the group
    if (!(groups_is_user_member(get_current_user_id(), $group->id) || bp_group_is_admin())) {
        return;
    }

    if (isset($bp->groups->current_group->slug)) {
        bp_core_new_subnav_item(array(
            'name' => 'Individual Steps',
            'slug' => 'steps_competitive',
            'parent_slug' => $bp->groups->current_group->slug,
            'parent_url' => bp_get_group_permalink($group),
            'screen_function' => 'innovage_pedometer_competitive_individual_show_screen',
            'position' => 40));
    }
}

add_action('bp_setup_nav', 'innovage_pedometer_add_competitive_individual_steps_page');

function innovage_pedometer_competitive_individual_show_screen() {

    add_action('bp_template_title', 'innovage_pedometer_competitive_individual_show_screen_title');
    add_action('bp_template_content', 'innovage_pedometer_competitive_individual_show_screen_content');

    $templates = array('groups/single/plugins.php', 'plugin-template.php');
    if (strstr(locate_template($templates), 'groups/single/plugins.php')) {
        bp_core_load_template(apply_filters('bp_core_template_plugin', 'groups/single/plugins'));
    } else {
        bp_core_load_template(apply_filters('bp_core_template_plugin', 'plugin-template'));
    }
}

function innovage_pedometer_competitive_individual_show_screen_title() {
    echo 'Steps';
}

function innovage_pedometer_competitive_individual_show_screen_content() {

    if (!is_user_logged_in()) {
        echo 'Please log in to view this content';
        return;
    }

    global $bp;
    $group = $bp->groups->current_group;

    // Get the total number of steps in the challenge
    if (function_exists('innovage_routes_get_goal_amount')) {
        $route_id = groups_get_groupmeta($group->id, 'challenge-type');
        $total_steps = innovage_routes_get_goal_amount($route_id);
    }

    // Start date is the date the group was created on
    // We want to get this from the start of the day - so remove the time
    $start_date = strtok($group->date_created, " ");

    // End date is the challenge-end-date as set out by the group metadata
    $end_date = groups_get_groupmeta($group->id, 'challenge-end-date');
    echo "<h2>Competitive challenge</h2>";

    if (function_exists('innovage_routes_get_goal_amount')) {
        $current_user_id = get_current_user_id();
        $user_steps = innovage_pedometer_get_user_steps($current_user_id, $start_date, $end_date);

        $percentage_complete = intval(($user_steps / $total_steps) * 100);
        $progress_visualisation = innovage_partner_get_visualisation($route_id, $percentage_complete);
        // Display the visualisation
        echo $progress_visualisation;
    }

    $results = innovage_pedometer_bp_group_individual_steps($group->id, $start_date, $end_date);

    echo "<table><tr><th>User</th><th>Steps</th><th></th></tr>";
    if (isset($results)) {
        foreach ($results as $row) {
            $user = get_userdata($row['id']);
            echo '<tr><td>' . $user->display_name . '</td><td>' . $row['step_count'] . '</td>';
            if (isset($total_steps)) {
                $percentage = intval(($row['step_count'] / $total_steps) * 100);
                echo '<td>' . $percentage . '%<div class="progressbar_individual">' . $percentage . '</div></td>';
            }
            echo '</tr>';
        }
    }
    echo "</table>";
}
