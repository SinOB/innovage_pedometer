<?php
/* TODO
 * as using jqplot in innovage_partner combine partner and pedometer into single plugin
 * put in google maps visualisation
 */

function innovage_partner_add_groups_collaborative_steps_page() {
    global $bp;


    if (!isset($bp->groups->current_group)) {
        return;
    }

    $group = $bp->groups->current_group;

    if (!isset($group) || !isset($group->id)) {
        return;
    }

    if (!bp_is_groups_component()) {
        return;
    }
    // Only display this tab once the user is a member of the group
    if (!(groups_is_user_member(get_current_user_id(), $group->id) || bp_group_is_admin())) {
        return;
    }

    // If challenge is not collaborative dyad type return
    $challenge_type = groups_get_groupmeta($group->id, 'challenge-approach');
    if ($challenge_type != 2) {
        return;
    }

    if (isset($bp->groups->current_group->slug)) {
        bp_core_new_subnav_item(array(
            'name' => 'Combined Steps',
            'slug' => 'combined_steps',
            'parent_slug' => $bp->groups->current_group->slug,
            'parent_url' => bp_get_group_permalink($bp->groups->current_group),
            'screen_function' => 'innovage_partner_collaborative_show_screen',
            'position' => 41));
    }
}

add_action('bp_setup_nav', 'innovage_partner_add_groups_collaborative_steps_page');

function innovage_partner_collaborative_show_screen() {

    add_action('bp_template_title', 'innovage_partner_collaborative_show_screen_title');
    add_action('bp_template_content', 'innovage_partner_collaborative_show_screen_content');

    $templates = array('groups/single/plugins.php', 'plugin-template.php');
    if (strstr(locate_template($templates), 'groups/single/plugins.php')) {
        bp_core_load_template(apply_filters('bp_core_template_plugin', 'groups/single/plugins'));
    } else {
        bp_core_load_template(apply_filters('bp_core_template_plugin', 'plugin-template'));
    }
}

function innovage_partner_collaborative_show_screen_title() {
    echo 'Partner Steps';
}

function innovage_partner_collaborative_show_screen_content() {

    if (!is_user_logged_in()) {
        echo 'Please log in to view this content';
        return;
    }

    global $bp;
    $group = $bp->groups->current_group;
    $start_date = strtok($group->date_created, " ");
    $end_date = groups_get_groupmeta($group->id, 'challenge-end-date');
    $route_id = groups_get_groupmeta($group->id, 'challenge-type');
    $total_steps = innovage_routes_get_goal_amount($route_id);


    // Only display visualisation if user has a partner in this group
    $current_user_id = get_current_user_id();
    $partner_info = innovage_partner_get_group_partner($current_user_id, $group->id);
    if (isset($partner_info) && !empty($partner_info)) {
        $partner_id = $partner_info->user_id;

        $percentage_complete = innovage_partner_get_dyad_percentage($group->id, $current_user_id, $partner_id);
        $progress_visualisation = innovage_partner_get_visualisation($route_id, $percentage_complete);
        // Display the visualisation
        echo $progress_visualisation;
    }

    $results = innovage_partner_collaborative_dyad_steps($group->id, $start_date, $end_date);
    $steps = array();
    $labels = array();

    # Get pair steps - remember steps calculated for a pair are not the same as
    # just adding individual steps for a member. They follow the formula (A+B)+sqrt(A*B)
    $total_pair_steps = 0;
    if (isset($results)) {
        foreach ($results as $team_data) {
            $steps[] = '[' . $team_data['pair_steps'] . ']';
            $total_pair_steps += $team_data['pair_steps'];
            $label = '';
            foreach ($team_data['members'] as $id => $individual_steps) {
                $user = get_userdata($id);
                $label .= $user->display_name . ' and ';
            }
            $labels[] = "'" . substr(trim($label), 0, -3) . "'";
        }
    }

    // Display all steps walked - even if walk more than required by group challenge
    $map_limit = $total_steps;
    if ($total_pair_steps > $total_steps) {
        $map_limit = $total_pair_steps;
    }
    // add at 5% buffer to the top of the map
    $map_limit = intval($map_limit * (105 / 100));

    echo '<h3>This group is aiming to walk a collaborative distance of ' . $total_steps . ' steps</h3>';
    echo '<p>So far the dyads in this group have walked a collaborative distance of ' . $total_pair_steps . ' steps</p>'
    ?><div id="chart3" style="width:300px;height:500px; "></div>
    <script type="text/javascript">
        jQuery(document).ready(function () {

            plot3 = jQuery.jqplot('chart3', [<?php echo implode(',', $steps) ?>], {
                // Tell the plot to stack the bars.
                stackSeries: true,
                captureRightClick: true,
                seriesDefaults: {
                    renderer: jQuery.jqplot.BarRenderer,
                    rendererOptions: {
                        // Highlight bars when mouse button pressed.
                        // Disables default highlighting on mouse over.
                        highlightMouseDown: false,
                        fillToZero: false,
                        barWidth: 150
                    },
                    pointLabels: {show: false}
                },
                grid: {
                    drawGridlines: true,
                    background: "transparent",
                    shadow: false,
                    borderWidth: 0
                },
                axesDefaults: {
                },
                axes: {
                    xaxis: {
                        show: false,
                        min: 0,
                        max: 2,
                        ticks: [],
                        numberTicks: undefined,
                        renderer: jQuery.jqplot.LinearAxisRenderer,
                        tickOptions: {
                            mark: 'outside',
                            showMark: false,
                            showGridline: false,
                            show: false,
                            showLabel: false,
                            formatString: ''
                        },
                        showTicks: false,
                        showTickMarks: true
                    },
                    yaxis: {
                        // Don't pad out the bottom of the data range.  By default,
                        padMin: 0,
                        // Only display int values
                        tickOptions: {
                            formatString: '%d'
                        },
                        min: 0,
                        // Make sure we show to the target step count
                        max: <?php echo $map_limit ?>
                    }
                },
                canvasOverlay: {
                    show: true,
                    objects: [
                        {horizontalLine: {
                                name: 'Goal',
                                y: <?php echo $total_steps ?>,
                                lineWidth: 5,
                                color: 'rgb(0, 0, 0)',
                                shadow: false
                            }}
                    ]
                },
                legend: {
                    renderer: jQuery.jqplot.EnhancedLegendRenderer,
                    show: true,
                    location: 'e',
                    placement: 'outside',
                    labels: [<?php echo implode(',', $labels) ?>],
                    rendererOptions: {
                        numberRows: 15,
                    }
                }
            });

        });</script>
    <?php
}
