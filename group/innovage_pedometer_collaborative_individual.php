<?php

/** /
 * Add Individual Steps Tab for collaborative challenge to the groups page.
 * @global type $bp
 */
function innovage_pedometer_add_groups_collaborative_individual_steps_page() {
    global $bp;

    if (!isset($bp->groups->current_group)) {
        return;
    }

    if (!bp_is_groups_component()) {
        return;
    }

    $group = $bp->groups->current_group;

    if (!isset($group) || !isset($group->id)) {
        return;
    }
    // If challenge is not collaborative dyad type return
    $challenge_type = groups_get_groupmeta($group->id, 'challenge-approach');
    if (!($challenge_type == 2 || $challenge_type == 4)) {
        return;
    }
    // Only display this tab once the user is a member of the group
    if (!(groups_is_user_member(get_current_user_id(), $group->id) || bp_group_is_admin())) {
        return;
    }

    if (isset($bp->groups->current_group->slug)) {
        bp_core_new_subnav_item(array(
            'name' => 'Individual Steps',
            'slug' => 'steps_collaborative',
            'parent_slug' => $bp->groups->current_group->slug,
            'parent_url' => bp_get_group_permalink($group),
            'screen_function' => 'innovage_pedometer_collaborative_individual_show_screen',
            'position' => 40));
    }
}

add_action('bp_setup_nav', 'innovage_pedometer_add_groups_collaborative_individual_steps_page');

function innovage_pedometer_collaborative_individual_show_screen() {

    add_action('bp_template_title', 'innovage_pedometer_collaborative_individual_show_screen_title');
    add_action('bp_template_content', 'innovage_pedometer_collaborative_individual_show_screen_content');

    $templates = array('groups/single/plugins.php', 'plugin-template.php');
    if (strstr(locate_template($templates), 'groups/single/plugins.php')) {
        bp_core_load_template(apply_filters('bp_core_template_plugin', 'groups/single/plugins'));
    } else {
        bp_core_load_template(apply_filters('bp_core_template_plugin', 'plugin-template'));
    }
}

function innovage_pedometer_collaborative_individual_show_screen_title() {
    echo 'Steps';
}

function innovage_pedometer_collaborative_individual_show_screen_content() {

    if (!is_user_logged_in()) {
        echo 'Please log in to view this content';
        return;
    }

    global $bp;
    $group = $bp->groups->current_group;

    // Get the total number of steps in the challenge
    if (function_exists('innovage_routes_get_goal_amount')) {
        $route_id = groups_get_groupmeta($group->id, 'challenge-type');
        $total_steps = innovage_routes_get_goal_amount($route_id);
    }

    // Start date is the date the group was created on
    // We want to get this from the start of the day - so remove the time
    $start_date = strtok($group->date_created, " ");

    // End date is the challenge-end-date as set out by the group metadata
    $end_date = groups_get_groupmeta($group->id, 'challenge-end-date');
    echo "<h2>Collaborative challenge</h2>";

    if (function_exists('innovage_routes_get_goal_amount')) {
        $current_user_id = get_current_user_id();
        $user_steps = innovage_pedometer_get_user_steps($current_user_id, $start_date, $end_date);

        $percentage_complete = intval(($user_steps / $total_steps) * 100);
        $progress_visualisation = innovage_partner_get_visualisation($route_id, $percentage_complete);
        // Display the visualisation
        echo $progress_visualisation;
    }

    $results = innovage_pedometer_bp_group_individual_steps($group->id, $start_date, $end_date);
    $steps = array();
    $labels = array();
    $total_user_steps = 0;
    if (isset($results)) {
        foreach ($results as $row) {
            $user = get_userdata($row['id']);
            $labels[] = "'" . $user->display_name . "'";
            $steps[] = '[' . $row['step_count'] . ']';
            $total_user_steps +=$row['step_count'];
        }
    }
    // Display all steps walked - even if walk more than required by group challenge
    $map_limit = $total_steps;
    if ($total_user_steps > $total_steps) {
        $map_limit = $total_user_steps;
    }
    // add at 5% buffer to the top of the map
    $map_limit = intval($map_limit * (105 / 100));

    echo '<h3>This group is aiming to walk a collaborative distance of ' . $total_steps . ' steps</h3>';
    echo '<p>So far this group have walked a collaborative distance of ' . $total_user_steps . ' steps</p>'
    ?><div id="chart3" style="width:300px;height:500px; "></div>
    <script type="text/javascript">
        var legendLabels = [<?php echo implode(',', $labels) ?>];
        jQuery(document).ready(function () {

            plot3 = jQuery.jqplot('chart3', [<?php echo implode(',', $steps) ?>], {
                // Tell the plot to stack the bars.
                stackSeries: true,
                captureRightClick: true,
                seriesDefaults: {
                    renderer: jQuery.jqplot.BarRenderer,
                    rendererOptions: {
                        // Highlight bars when mouse button pressed.
                        // Disables default highlighting on mouse over.
                        highlightMouseDown: false,
                        fillToZero: false,
                        barWidth: 150
                    },
                    pointLabels: {show: false}
                },
                grid: {
                    drawGridlines: true,
                    background: "transparent",
                    shadow: false,
                    borderWidth: 0
                },
                axesDefaults: {
                },
                axes: {
                    xaxis: {
                        show: false,
                        min: 0,
                        max: 2,
                        ticks: [],
                        numberTicks: undefined,
                        renderer: jQuery.jqplot.LinearAxisRenderer,
                        tickOptions: {
                            mark: 'outside',
                            showMark: false,
                            showGridline: false,
                            show: false,
                            showLabel: false,
                            formatString: ''
                        },
                        showTicks: false,
                        showTickMarks: true
                    },
                    yaxis: {
                        // Don't pad out the bottom of the data range.  By default,
                        padMin: 0,
                        // Only display int values
                        tickOptions: {
                            formatString: '%d'
                        },
                        min: 0,
                        // Make sure we show to the target step count
                        max: <?php echo $map_limit ?>
                    }
                },
                canvasOverlay: {
                    show: true,
                    objects: [
                        {horizontalLine: {
                                name: 'Goal',
                                y: <?php echo $total_steps ?>,
                                lineWidth: 5,
                                color: 'rgb(0, 0, 0)',
                                shadow: false
                            }}
                    ]
                },
                legend: {
                    renderer: jQuery.jqplot.EnhancedLegendRenderer,
                    show: true,
                    location: 'e',
                    placement: 'outside',
                    labels: legendLabels,
                    rendererOptions: {
                        numberRows: 15,
                    }
                }
            });

        });</script>
    <?php
}
