<?php

function innovage_partner_add_groups_individual_steps_page() {
    global $bp;

    if (!isset($bp->groups->current_group)) {
        return;
    }

    $group = $bp->groups->current_group;

    if (!isset($group) || !isset($group->id)) {
        return;
    }

    if (!bp_is_groups_component()) {
        return;
    }
    // Only display this tab once the user is a member of the group
    if (!(groups_is_user_member(get_current_user_id(), $group->id) || bp_group_is_admin())) {
        return;
    }

    // If challenge is not competitive dyad type return 
    $challenge_type = groups_get_groupmeta($group->id, 'challenge-approach');
    if ($challenge_type != 1) {
        return;
    }

    if (isset($bp->groups->current_group->slug)) {
        bp_core_new_subnav_item(array(
            'name' => 'Partner Steps',
            'slug' => 'partner_steps',
            'parent_slug' => $bp->groups->current_group->slug,
            'parent_url' => bp_get_group_permalink($bp->groups->current_group),
            'screen_function' => 'innovage_partner_bp_group_show_screen',
            'position' => 41));
    }
}

add_action('bp_setup_nav', 'innovage_partner_add_groups_individual_steps_page');

function innovage_partner_bp_group_show_screen() {

    add_action('bp_template_title', 'innovage_partner_bp_group_show_screen_title');
    add_action('bp_template_content', 'innovage_partner_bp_group_show_screen_content');

    $templates = array('groups/single/plugins.php', 'plugin-template.php');
    if (strstr(locate_template($templates), 'groups/single/plugins.php')) {
        bp_core_load_template(apply_filters('bp_core_template_plugin', 'groups/single/plugins'));
    } else {
        bp_core_load_template(apply_filters('bp_core_template_plugin', 'plugin-template'));
    }
}

function innovage_partner_bp_group_show_screen_title() {
    echo 'Partner Steps';
}

function innovage_partner_bp_group_show_screen_content() {

    if (!is_user_logged_in()) {
        echo 'Please log in to view this content';
        return;
    }

    global $bp;
    $group = $bp->groups->current_group;

    // Start date is the date the group was created on
    // We want to get this from the start of the day - so remove the time
    $start_date = strtok($group->date_created, " ");

    // End date is the challenge-end-date as set out by the group metadata
    $end_date = groups_get_groupmeta($group->id, 'challenge-end-date');

    // Get the total number of steps in the challenge
    // and display visualisation
    if (function_exists('innovage_routes_get_goal_amount')) {
        $route_id = groups_get_groupmeta($group->id, 'challenge-type');
        $total_steps = innovage_routes_get_goal_amount($route_id);

        $current_user_id = get_current_user_id();
        $partner_info = innovage_partner_get_group_partner($current_user_id, $group->id);

        // Only display visualisation if user has a partner in this group
        if (isset($partner_info) && !empty($partner_info)) {
            $partner_id = $partner_info->user_id;

            $percentage_complete = innovage_partner_get_dyad_percentage($group->id, $current_user_id, $partner_id);
            $progress_visualisation = innovage_partner_get_visualisation($route_id, $percentage_complete);
            // Display the visualisation
            echo $progress_visualisation;
        }
    }

    $results = innovage_partner_basic_dyad_steps($group->id, $start_date, $end_date);
    echo "<h3>The table below shows the collective steps walked by each partnership</h3>";
    echo "<table><tr><th>Partners</th><th>Team steps</th><th></th></tr>";
    if (isset($results)) {
        foreach ($results as $team_data) {
            echo '<tr><td>';
            foreach ($team_data['members'] as $user_id => $steps) {
                $user = get_userdata($user_id);
                echo $user->display_name . " ($steps Steps) ";
            }
            echo '</td><td>' . $team_data['pair_steps'] . '</td>';
            if (isset($total_steps)) {
                $percentage = intval(($team_data['pair_steps'] / $total_steps) * 100);
                echo '<td style="width:200px;">' . $percentage . '%<div class="progressbar">' . $percentage . '</div></td>';
            }
            echo '</tr>';
        }
    }
    echo "</table>";
}
