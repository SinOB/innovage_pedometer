<?php

function innovage_pedometer_graphs_add_scripts() {
    global $is_IE;

    if ($is_IE) {
        wp_register_script('excanvas', plugins_url('src/excanvas.min.js', __FILE__));
        wp_enqueue_script('excanvas');
    }

    wp_register_script('jqplot', plugins_url('src/jquery.jqplot.js', __FILE__));
    wp_enqueue_script('jqplot');

    wp_register_script('bar', plugins_url('src/plugins/jqplot.barRenderer.js', __FILE__));
    wp_enqueue_script('bar');

    wp_register_script('cax', plugins_url('src/plugins/jqplot.categoryAxisRenderer.js', __FILE__));
    wp_enqueue_script('cax');

    wp_register_script('cax', plugins_url('src/plugins/jqplot.pointLabels.js', __FILE__));
    wp_enqueue_script('pol');

    wp_register_script('legend', plugins_url('src/plugins/jqplot.enhancedLegendRenderer.min.js', __FILE__));
    wp_enqueue_script('legend');

    wp_register_script('canv', plugins_url('src/plugins/jqplot.canvasOverlay.min.js', __FILE__));
    wp_enqueue_script('canv');
}

function innovage_pedometer_graphs_add_css() {
    echo '<link type="text/css" rel="stylesheet" href="' . plugins_url('src/jquery.jqplot.css', __FILE__) . '" />';
}

add_action('wp_enqueue_scripts', 'innovage_pedometer_graphs_add_scripts');
add_action('wp_head', 'innovage_pedometer_graphs_add_css');

function innovage_pedometer_graph_member($user_id, $group_id) {

    $challenge_type = groups_get_groupmeta($group_id, 'challenge-approach');
    $group = groups_get_group(array('group_id' => $group_id));
    $challenge_types = innovage_bp_get_challenge_types();
    $route_id = groups_get_groupmeta($group_id, 'challenge-type');
    $route = innovage_routes_get_route($route_id);
    $route_title = $route->title;
    $total_steps = innovage_routes_get_goal_amount($route_id);
    $end_date = groups_get_groupmeta($group_id, 'challenge-end-date');
    $start_date = strtok($group->date_created, " ");
    $my_steps = innovage_pedometer_get_user_steps($user_id, $start_date, $end_date);
    $partner_info = innovage_partner_get_group_partner($user_id, $group_id);

    $user = get_userdata($user_id);
    $user_name = $user->display_name;

    $data = "['" . $user_name . "', " . $my_steps . "]";
    // Only display visualisation if user has a partner in this group
    if (isset($partner_info) && !empty($partner_info)) {
        $partner_id = $partner_info->user_id;
        $partner_steps = innovage_pedometer_get_user_steps($partner_id, $start_date, $end_date);

        $data .=",['Partner', " . $partner_steps . "]";

        // If challenge is competitive dyad type
        if ($challenge_type == 1) {
            $dyad_total = innovage_partner_basic_dyad_steps_pair($my_steps, $partner_steps);
            $data .= ",['" . $user_name . " & Partner', " . $dyad_total . "]";
        }
        // If challenge is collaborative dyad challenge type
        else if ($challenge_type == 2) {
            $dyad_total = innovage_partner_collaborative_dyad_steps_pair($my_steps, $partner_steps);
            $data .= ",['" . $user_name . " & Partner', " . $dyad_total . "]";

            // If a collaborative challenge show the groups progess
            $collaborative_walked = innovage_partner_collaborative_dyad_steps_combined($group_id, $start_date, $end_date);
            $data .=",['Group Walked', " . $collaborative_walked . "]";
        }
    }

    $title = $group->name . ' ' . $route_title . ' (' . $challenge_types[$challenge_type];

    echo '<script>
    jQuery(document).ready(function(){
    var line1 = [' . $data . '];
    var plot1 = jQuery.jqplot(\'chart1_' . $group_id . '\', [line1], {
        seriesDefaults:{
            renderer:jQuery.jqplot.BarRenderer,
        },
        legend: {
            show: false,
        },
        axes: {
            // Use a category axis on the x axis and use our custom ticks.
            xaxis: {
                renderer: jQuery.jqplot.CategoryAxisRenderer,
            },
            // Pad the y axis just a little so bars can get close to, but
            // not touch, the grid boundaries.  1.2 is the default padding.
            yaxis: {
                min:0,
                pad: 1.05,
                tickOptions: {
                    formatString: \'%d\'
                },
                max:' . intval($total_steps + ($total_steps / 100 * 5)) . '
            }
        },
        canvasOverlay: {
            show: true,
            objects: [
                {horizontalLine: {
                    name: \'Goal\',
                    y: ' . $total_steps . ',
                    lineWidth: 5,
                    color: \'rgb(0, 0, 0)\',
                    shadow: false
                }}
            ]
        }
    });
});
</script>
<h2>' . $title . '</h2>
<div id="chart1_' . $group_id . '" style="width:600px; height:250px;"></div>
';
}
