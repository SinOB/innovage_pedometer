<?php

/*
  Plugin Name: Innovage Pedometer
  Plugin URI: https://bitbucket.org/SinOB/innovage_pedometer
  Description: Basic user step counter form and pages for group challenge step counts
  Author: Sinead O'Brien
  Version: 1.2
  Author URI: https://bitbucket.org/SinOB
  Requires at least: WP 3.9, BuddyPress 2.0.1
  Tested up to: WP 4.1, BuddyPress 2.0.3
  License: GNU General Public License 2.0 (GPL) http://www.gnu.org/licenses/gpl.html
 */

global $innovage_pedometer_db_version;
$innovage_pedometer_db_version = "1.0";

global $innovage_partner_db_version;
$innovage_partner_db_version = "0.2";

$innopd_errorMessages = new WP_Error();

register_activation_hook(__FILE__, 'innovage_pedometer_install');

add_action('wp_footer', 'stepcounter_print_scripts');

/** /
 * Only load the BuddyPress plugin functions if BuddyPress is loaded and initialized.
 */
function innovage_pedometer_init() {

    if (function_exists('bp_is_active')) {
        require( dirname(__FILE__) . '/innovage_pedometer_functions.php' );
        require( dirname(__FILE__) . '/innovage_pedometer_partner_functions.php' );

        if (bp_is_active('groups')) {
            // Add new tab to Group for competitive individual steps
            require( dirname(__FILE__) . '/group/innovage_pedometer_competitive_individual.php' );

            // Add new tab to Group for collaborative individual steps
            require( dirname(__FILE__) . '/group/innovage_pedometer_collaborative_individual.php' );

            // Add a new tab to Group for competitive dyad challenge
            require( dirname(__FILE__) . '/group/innovage_partner_competitive_dyad.php' );

            // Add a new tab to Group for the collaborative dyad challenge
            require( dirname(__FILE__) . '/group/innovage_partner_collaborative_dyad.php' );
        }
        // Add new tab to Member/User to show Steps
        require( dirname(__FILE__) . '/member/innovage_pedometer_bp_member.php' );

        require( dirname(__FILE__) . '/innovage_pedometer_graphs.php' );

        // Add a new tab to Group for Dyad administation - admin users only
        require( dirname(__FILE__) . '/group/innovage_pedometer_partner_admin.php' );
    }
}

add_action('bp_include', 'innovage_pedometer_init');

/** /
 * Create the database table when the plugin is installed.
 * Also create a db version so can perform table updates later if necessary.
 * @global type $wpdb
 * @global string $innovage_pedometer_db_version
 */
function innovage_pedometer_install() {
    global $wpdb;
    global $innovage_pedometer_db_version;
    global $innovage_partner_db_version;

    $table_name = $wpdb->prefix . "innovage_pedometer";

    if (get_option("innovage_pedometer_db_version") != $innovage_pedometer_db_version) {
        $sql = "CREATE TABLE $table_name (
          id bigint(20) unsigned NOT NULL,
          step_date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
          step_count Int(11) DEFAULT 0,
          created_date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
          created_by bigint(20) unsigned NOT NULL,
          UNIQUE KEY id (id, step_date)
            );";
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta($sql);
    }
    update_option("innovage_pedometer_db_version", $innovage_pedometer_db_version);


    if (get_option("innovage_partner_db_version") != $innovage_partner_db_version) {
        // Execute the update logic here
        $table1_name = $wpdb->prefix . "innovage_teams";
        $table2_name = $wpdb->prefix . "innovage_team_users";
        $table3_name = $wpdb->prefix . "innovage_partners_deleted";

        // Going to need 2 tables, 1 for the team and another for team members
        $sql1 = "CREATE TABLE $table1_name (
            id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
            group_id bigint(20) unsigned NOT NULL,
            date_created datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
            created_by bigint(20) unsigned NOT NULL,
            UNIQUE KEY id (id)
        );";

        $sql2 = "CREATE TABLE $table2_name (
            id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
            team_id bigint(20) unsigned NOT NULL,
            user_id bigint(20) unsigned NOT NULL,
            is_confirmed tinyint(1) DEFAULT 0,
            date_created datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
            created_by bigint(20) unsigned NOT NULL,
            UNIQUE KEY id (id)
        );";

        $sql3 = "CREATE TABLE $table3_name (
            id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
            user_id bigint(20) unsigned NOT NULL,
            team_id bigint(20) unsigned NOT NULL,
            group_id bigint(20) unsigned NOT NULL,
            created_date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
            created_by bigint(20) unsigned NOT NULL,
            deleted_date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
            deleted_by bigint(20) unsigned NOT NULL,
            UNIQUE KEY id (id)
            );";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta($sql1);
        dbDelta($sql2);
        dbDelta($sql3);

        // Then update the version value
        update_option("innovage_partner_db_version", $innovage_partner_db_version);
    }
}

/**
 * Used in test cases only. Drop all of the tables associated with this plugin
 */
function innovage_pedometer_uninstall() {
    global $wpdb;

    $table_name1 = $wpdb->prefix . "innovage_pedometer";
    $table_name2 = $wpdb->prefix . "innovage_teams";
    $table_name3 = $wpdb->prefix . "innovage_team_users";

    $wpdb->query("DROP TABLE IF EXISTS $table_name1");
    $wpdb->query("DROP TABLE IF EXISTS $table_name2");
    $wpdb->query("DROP TABLE IF EXISTS $table_name3");
}
