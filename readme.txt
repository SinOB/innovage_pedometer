=== Innovage Pedometer ===
Contributors: SinOB
Tags: buddypress
Requires at least: WP 3.9, BuddyPress 2.0.1
Tested up to: WP 4.1, BuddyPress 2.0.3
Stable tag: 1.2
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Allows a logged in user to keep track of the number of steps they have walked 
each day. Also allows users to form 1-to-1 partnerships.

== Description ==

Innovage pedometer is a BuddyPress plugin to allow a logged in user to keep track 
of their steps and partner up with other users. It allows logged in users to
enter their step count for a day. 

To the user profile the following tabs and pages are added;
 * Steps: Page to allow user or their partner to add steps. Displays steps taken 
over the last 30 days. Also displays a graph for each of the challenges a user 
has joined.

To the group profile the following tabs and pages are added;
 * Individual steps: Shows animated map of users progress and graph of progress 
for all users.
 * Partner steps Shows animated map of users dyad and a graph of progress for 
all dyads. This is only available on dyad challenges.
 * Partners : Allows group owner and users with edit permissions 
(editor/administrator) to create partnerships within a group.

Animated maps are created and managed via separate plugin: innovage_bp_group_challenge

There are currently no admin options.


== Installation ==

1. Download
2. Upload to your '/wp-contents/plugins/' directory.
3. Activate the plugin through the 'Plugins' menu in WordPress.
