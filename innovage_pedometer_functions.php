<?php

/** /
 * Embed the necessary javascript and css scripts on a page
 * if they have not already been included.
 */
function innovage_pedometer_enqueue_styles_scripts() {
    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_script('innovage-pedometer-js', plugins_url('innovage_pedometer.js', __FILE__));
    wp_enqueue_style('jquery-ui-css', plugins_url('css/jquery-ui.css', __FILE__));
    wp_enqueue_script('jquery-ui-progressbar');
}

add_action("wp_enqueue_scripts", "innovage_pedometer_enqueue_styles_scripts");

/** /
 * Get a count of all of the users steps
 *
 * @global type $wpdb
 * @param type $user_id
 * @return int
 */
function innovage_pedometer_get_user_steps_total($user_id) {
    global $wpdb;
    $table_name = $wpdb->prefix . "innovage_pedometer";

    $sql = $wpdb->prepare("SELECT sum(step_count) as step_cnt "
            . "FROM $table_name "
            . "WHERE id= %s ", $user_id);
    $result = $wpdb->get_row($sql);
    if (isset($result->step_cnt) && $result->step_cnt != null) {
        return $result->step_cnt;
    } else {
        return 0;
    }
}

/** /
 * Display the pedometer form and/or save walking data on submit
 *
 * @param type $is_partner
 * @return string
 */
function innovage_pedometer_step_form($is_partner) {

    // If user isn't logged in hide the shortcode
    if (!is_user_logged_in()) {
        return 'Please log in to view this content';
    }

    $displayed_user_id = bp_displayed_user_id();

    // If have POST save data
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST["innopd_step_date"])) {
            $step_date = sanitize_text_field($_POST["innopd_step_date"]);
            innovage_pedometer_delete_user_date($step_date, $displayed_user_id);
        } else {
            stepcounter_save_data($_POST, $is_partner);
        }
    }

    $sdstr = innovage_pedometer_show_form($is_partner);

    $sdstr .= innovage_pedometer_view_previous_steps($is_partner);
    return $sdstr;
}

/** /
 * Get last 30 days worth of steps for current user
 *
 * @global type $wpdb
 * @param type $is_partner
 * @return string
 */
function innovage_pedometer_view_previous_steps($is_partner) {
    global $wpdb;
    $table_name = $wpdb->prefix . "innovage_pedometer";
    $displayed_user_id = bp_displayed_user_id();
    $current_user_id = get_current_user_id();

    //If not the current user profile or the current users partner profile 
    //or the current user has admin or author role return
    if (!($displayed_user_id == $current_user_id || $is_partner == 1 || current_user_can('edit_pages'))) {
        return;
    }

    $sql = $wpdb->prepare("SELECT step_count,"
            . " date(step_date) as step_date"
            . " FROM $table_name WHERE id= %s"
            . " ORDER BY step_date DESC LIMIT 30", $displayed_user_id);

    $retrieve_data = $wpdb->get_results($sql);

    if (isset($retrieve_data) && !empty($retrieve_data)) {
        // Get step counts for any available dates
        if ($displayed_user_id == $current_user_id) {
            $form_heading = 'Steps taken by you over the last 30 days';
        } else if ($is_partner == 1 || current_user_can('edit_pages')) {
            $form_heading = 'Steps taken by ' . bp_core_get_userlink($displayed_user_id) . ' over the last 30 days';
        }
        $str = '<h2>' . $form_heading . '</h2>'
                . '<table><tr><th>Date</th><th>Steps</th><th>Delete</th></tr>';

        $last_updated = null;
        foreach ($retrieve_data as $retrieved_data) {
            $str .= '<tr><td>' . $retrieved_data->step_date . '</td>'
                    . '<td>' . $retrieved_data->step_count . '</td>'
                    . '<td>'
                    . '<a href="javascript:submitInnopdDeleteForm(\'' . $retrieved_data->step_date . '\')">'
                    . '<img alt="Delete the steps for this day" title="Delete the steps for this day" src=' . plugins_url('images/remove.png', __FILE__) . '></a></td>'
                    . '</tr>';
        }
        if (isset($retrieve_data) && !empty($retrieve_data)) {
            $last_updated = $retrieve_data[0]->step_date;
        }
        $str .= "</table>";

        if (isset($last_updated) && !empty($last_updated)) {
            // add a day to last updated as want to default to the NEXT day
            // not overwrite the data for the last day
            $oDate = new DateTime($last_updated);
            $oDate->add(new DateInterval('P1D'));
            $last_updated = $oDate->format('Y-m-d');
            $str .= "<input type='hidden' value='" . $last_updated . "' id='last_updated' name='last_updated'>";
        }
        return $str;
    }
}

/** /
 * Display the error messages to screen
 *
 * @global WP_Error $innopd_errorMessages
 */
function innovage_pedometer_printErrorMessages($innopd_errorMessages) {
    if (is_wp_error($innopd_errorMessages)) {
        $errors = $innopd_errorMessages->get_error_messages();
        echo "<br/><span class='innovage_error'>";
        foreach ($errors as $error) {
            echo 'Error: ' . $error;
        }
        echo "</span>";
    }
}

/** /
 * Delete a user row from the innovage_pedometer table
 *
 * @global type $wpdb
 * @param type $date
 * @param type $user_id
 */
function innovage_pedometer_delete_user_date($date, $user_id) {
    global $wpdb;
    $table_name = $wpdb->prefix . "innovage_pedometer";
    $wpdb->delete($table_name, array('id' => $user_id, 'step_date' => $date));
}

/** /
 * Get a sum of the users steps over a certain time frame
 *
 * @global type $wpdb
 * @param type $user_id
 * @param type $start_date
 * @param type $end_date
 * @return int
 */
function innovage_pedometer_get_user_steps($user_id, $start_date, $end_date) {
    global $wpdb;
    $table_name = $wpdb->prefix . "innovage_pedometer";

    $sql = $wpdb->prepare("SELECT sum(step_count) as step_cnt "
            . "FROM $table_name "
            . "WHERE id=%s "
            . "AND (step_date >= '$start_date' || step_date is NULL) "
            . "AND (step_date <='$end_date' || step_date is NULL) ", $user_id);

    $result = $wpdb->get_row($sql);
    if (isset($result->step_cnt) && $result->step_cnt != null) {
        return $result->step_cnt;
    } else {
        return 0;
    }
}

/** /
 * HTML for the pedometer form
 *
 * @param type $is_partner
 */
function innovage_pedometer_show_form($is_partner) {

    $current_user_id = get_current_user_id();
    $displayed_user_id = bp_displayed_user_id();

    //If not the current user profile or the current users partner profile 
    //or the current user has admin or author role return
    if (!($displayed_user_id == $current_user_id || $is_partner == 1 || current_user_can('edit_pages'))) {
        return;
    }

    $form_title = "";
    if ($displayed_user_id == $current_user_id) {
        $form_title = "Add your step count here";
    } else if ($is_partner == 1) {
        $form_title = "Add steps for " . bp_core_get_userlink($displayed_user_id) . " here";
    } else if (current_user_can('edit_pages')) {
        $form_title = "Add steps for " . bp_core_get_userlink($displayed_user_id) . " here";
    }
    ?>
    <form method="post">
        <h2><?php echo $form_title ?></h2>
        <p>
            <label> Number of steps:</label><BR>
            <input type="text" 
                   name="c_steps_count"
                   id="c_steps_count"
                   maxlength=9
                   size=20
                   required="required" 
                   data-required="true"> </p>

        <p>
            <label>Date from</label><BR>
            <input type="text"
                   name="c_steps_date_start"
                   id="c_steps_date_start"
                   class="custom_date"
                   required="required" > 
            to
            <input type="text"
                   name="c_steps_date_end"
                   id="c_steps_date_end"
                   class="custom_date"
                   required="required" > in the format YYYY-MM-DD
        </p>

        <p><input type="submit" name="submit" id="submit" value="Save Steps"> </p>

    </form>
    <form method="post" id="innopd_delete_step">
        <input type="hidden" name="innopd_step_date" id="innopd_step_date" value="">
    </form>

    <?php
}

/** /
 * Do some user input data validation and sanitation.
 * @global WP_Error $innopd_errorMessages
 * @return type
 */
function stepcounter_clean_add_input() {
    global $innopd_errorMessages;

    $date_format = "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/";

    if (!isset($_POST['c_steps_count']) || !is_numeric($_POST['c_steps_count'])) {
        $innopd_errorMessages->add('required', __('The number of steps is a required field and must be a number !'));
        return;
    }

    if (!isset($_POST['c_steps_date_start']) || empty($_POST['c_steps_date_start']) || !preg_match($date_format, $_POST['c_steps_date_start'])) {
        $innopd_errorMessages->add('required', __('The from date is a required field and must be in the format YYYY-MM-DD'));
        return;
    }
    if (!isset($_POST['c_steps_date_end']) || empty($_POST['c_steps_date_end']) || !preg_match($date_format, $_POST['c_steps_date_end'])) {
        $innopd_errorMessages->add('required', __('The to date is a required field and must be in the format YYYY-MM-DD'));
        return;
    }

    if ($_POST['c_steps_count'] > 999999999) {
        $innopd_errorMessages->add('required', __('The number of steps is too high. It has an upper limit of 999999999 !'));
        return;
    }

    $start_date = new DateTime($_POST['c_steps_date_start']);
    $end_date = new DateTime($_POST['c_steps_date_end']);
    $current_date = new DateTime();

    if ($start_date > $current_date) {
        $innopd_errorMessages->add('required', __('The from date cannot be in the future!'));
        return;
    }
    if ($end_date > $current_date) {
        $innopd_errorMessages->add('required', __('The to date cannot be in the future!'));
        return;
    }

    // sanitize the date
    $_POST['c_steps_date_start'] = sanitize_text_field($_POST['c_steps_date_start']);
    $_POST['c_steps_date_end'] = sanitize_text_field($_POST['c_steps_date_end']);

    // sanitize the step count
    $safe_count = intval($_POST['c_steps_count']);
    if (!$safe_count) {
        $safe_count = 0;
    }

    $_POST['c_steps_count'] = $safe_count;
}

/** /
 * Save the step count to the custom table. Calculates the steps to add per day 
 * over the specified date range.
 * @global WP_Error $innopd_errorMessages
 * @global type $wpdb
 * @param type $postdata
 * @param type $is_partner
 * @return type
 */
function stepcounter_save_data($postdata, $is_partner) {

    global $innopd_errorMessages;

    // Only save if the user is logged in
    $current_id = get_current_user_id();

    if (!is_user_logged_in() || $current_id <= 0) {
        return;
    }

    stepcounter_clean_add_input();

    // Only continue with save if there are no errors
    if (!empty($innopd_errorMessages) && count($innopd_errorMessages->errors) > 0) {
        innovage_pedometer_printErrorMessages($innopd_errorMessages);
        return;
    }

    $displayed_user_id = bp_displayed_user_id();

    // Get count of days between start and end date
    $start_date = new DateTime($postdata['c_steps_date_start']);
    $end_date = new DateTime($postdata['c_steps_date_end']);

    $interval = innovage_pedometer_get_dates_interval($start_date, $end_date);

    // Divide total steps by number of days and convert from float to int
    $steps_per_day = intval($postdata['c_steps_count'] / $interval);

    // Quick hack to include the end date in the period
    $end_date->modify('+1 second');

    // For each date add those steps to the database
    $day_interval = DateInterval::createFromDateString('1 day');
    $period = new DatePeriod($start_date, $day_interval, $end_date);

    foreach ($period as $dt) {
        innovage_pedometer_update_steps($displayed_user_id, $dt, $steps_per_day);
    }

    if ($is_partner) {
        echo "<span class='innovage_message'>Thank you for updating your partners step count. Please remind them to reset their pedometer</span>";
    } else {
        echo "<span class='innovage_message'>Thank you for updating your step count. Please remember to reset your pedometer</span>";
    }
    return;
}

/** /
 * Update the user steps table with the appropriate steps for a specific day
 *
 * @global type $wpdb
 * @param type $user_id
 * @param type $date
 * @param type $step_count
 */
function innovage_pedometer_update_steps($user_id, $date, $step_count) {
    global $wpdb;

    $current_user_id = get_current_user_id();
    $table_name = $wpdb->prefix . "innovage_pedometer";
    $step_date = $date->format('Y-m-d 00:00:00');
    $wpdb->replace($table_name, array('id' => $user_id,
        'step_count' => $step_count,
        'step_date' => $step_date,
        'created_date' => current_time('mysql', true),
        'created_by' => $current_user_id));
}

/** /
 * Calculate the number of days between the start and end dates
 *
 * @param type $start_date
 * @param type $end_date
 * @return int
 */
function innovage_pedometer_get_dates_interval($start_date, $end_date) {
    $date_diff = date_diff($start_date, $end_date);
    $interval = $date_diff->format('%a');
    $interval += 1;
    return $interval;
}

/** /
 * Turn on the calendar datapicker for the pedometer form.
 * Force picker to not allow future dates.
 * Show the calendar image.
 * Use the calenadr image in the plugin file.
 */
function stepcounter_print_scripts() {
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('.custom_date').datepicker({
                maxDate: new Date,
                dateFormat: 'yy-mm-dd',
                showOn: "both",
                buttonImageOnly: true,
                buttonImage: "<?php echo plugins_url('images/calendar_icon.jpg', __FILE__) ?>"
            });
            // Only update if the input exists and has a value
            if ($('#last_updated').length && $('#last_updated').val().length)
            {
                $('#c_steps_date_start').datepicker('setDate', $('#last_updated').val());
            }
            $('#c_steps_date_end').datepicker('setDate', new Date());
        });
    </script>
    <?php
}

/** /
 * Get the indicidual steps for each member of a group between a specified date range
 *
 * @global type $wpdb
 * @param type $group_id
 * @param type $start_date
 * @param type $end_date
 * @return array
 */
function innovage_pedometer_bp_group_individual_steps($group_id, $start_date, $end_date) {
    global $wpdb;
    $table1_name = $wpdb->prefix . "innovage_pedometer";
    $table2_name = $wpdb->prefix . "bp_groups_members";

    $results = array();
    // Get the group members
    $query_members = $wpdb->prepare("SELECT user_id FROM $table2_name"
            . " WHERE group_id=%s ", $group_id);

    $members = $wpdb->get_results($query_members);

    $query_member_steps = "SELECT id, sum(step_count) as step_cnt"
            . " FROM $table1_name"
            . " WHERE (step_date >= '$start_date' || step_date is NULL) "
            . " AND (step_date <='$end_date' || step_date is NULL) "
            . " AND id= %s";

    // For each member get the step count
    for ($i = 0; $i < count($members); $i++) {
        $user_id = $members[$i]->user_id;
        $member_count = $wpdb->get_row($wpdb->prepare($query_member_steps, $user_id));
        $count = isset($member_count) && isset($member_count->step_cnt) ? $member_count->step_cnt : 0;
        array_push($results, array('id' => $user_id, 'step_count' => $count));
    }
    return $results;
}
