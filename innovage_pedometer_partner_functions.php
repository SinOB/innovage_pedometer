<?php
add_shortcode('show_innovage_partner_form', 'innovage_partner_view_add_page');

/** /
 * Display the form to allow a user to create/edit a partnership
 * 
 * @global type $bp
 * @param type $atts
 * @param type $content
 * @return string
 */
function innovage_partner_view_add_page($atts, $content = '') {
    global $bp;

    // If user isn't logged in hide the page
    if (!is_user_logged_in()) {
        return "Please log in to view this content";
    }

    $group_id = '';

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {

        if (isset($_POST["partner_group_id"])) {
            $group_id = $_POST["partner_group_id"];
        }

        // Only allow the member to see this page if they are a member of the group
        // For some reason the following doesn't work for non admin user ??
        // $group = new BP_Groups_Group($group_id);
        //if (!bp_group_is_member($group)) {
        if (!groups_is_user_member(get_current_user_id(), $group_id)) {
            return "Access denied. You are not a member of this group.";
        }

        $current_user_id = get_current_user_id();

        if (isset($_POST["innopt_partner_action"])) {
            // Save the partnership  
            if ($_POST["innopt_partner_action"] === 'add') {
                $partner_id = intval($_POST["innopt_partner_id"]);
                if ($partner_id != $current_user_id) {
                    innovage_partnership_save($partner_id, $group_id);
                }
            }
            // Delete the partnership
            else if ($_POST["innopt_partner_action"] === 'delete') {
                $team_id = intval($_POST["innopt_team_id"]);
                innovage_partnership_delete($team_id);
            }
        }

        // If user already has a partner display partner and do not allow
        // them to select another partner
        $partner_team_info = innovage_partner_get_group_partner($current_user_id, $group_id);

        if (isset($partner_team_info) && !empty($partner_team_info)) {
            $team_id = $partner_team_info->team_id;
            $partner_id = $partner_team_info->user_id;
            ?>
            <h1 class="entry-title">Your partner:</h1> 
            <p> You are currently partnered with:
                <?php echo bp_core_get_userlink($partner_id); ?>
                <?php echo bp_core_fetch_avatar(array('item_id' => $partner_id)); ?>
            </p>
            <p>If you would like to choose a different partner please unpartner from your current partner first. You can do this using the link below.</p>
            <a href='javascript:submitInnopdDeletePartnershipForm(<?php echo $team_id ?>)' class='innovage_button'>Unpartner</a>

            <?php
            innovage_make_partnership_form($group_id);
            return;
        }

        innovage_show_group_members($group_id);
    }
}

/** /
 * Output the make partnership form
 *
 * @param type $group_id
 */
function innovage_make_partnership_form($group_id) {
    ?>
    <form method="post" id="innopt_make_partnership">
        <input type="hidden" name = "partner_group_id" id="partner_group_id" value="<?php echo $group_id ?>">
        <input type="hidden" name = "innopt_team_id" id="innopt_team_id" value="">
        <input type="hidden" name="innopt_partner_action" id="innopt_partner_action" value="">
        <input type="hidden" name="innopt_partner_id" id="innopt_partner_id" value="">
    </form>
    <?php
}

/** /
 * Display the group members as possible partners on the page
 * 
 * @return type
 */
function innovage_show_group_members($group_id) {
    global $bp;
    if (bp_group_has_members('group_id=' . $group_id . '&exclude_admins_mods=0&per_page=1000')) :
        ?>
        <h1 class="entry-title">Find a partner:</h1>
        <p>Choose your partner from the list below. In order to choose a 
            partner they must be registered with iStep and a member of the same group challenge.
            It is not possible to parter with a member who already has a partner. These members will be marked as unavailable.</p>
        <form method='POST' action="<?php echo get_bloginfo('url'); ?>/registerspecial">
            <input type='hidden' name='group_id' value='<?php echo $group_id ?>' />
            <input type="submit" value="Create a special managed user partner account">
        </form>
        <br/>

        <ul id="member-list" class="item-list">
            <?php while (bp_group_members()) : bp_group_the_member(); ?>

                <li>
                    <!-- Example template tags you can use -->
                    <?php bp_group_member_avatar() ?>
                    <?php bp_group_member_link() ?>
                    <?php bp_group_member_joined_since() ?>

                    <?php
                    // DO not show partner option if user is in the group
                    if (innovage_partner_user_has_group_partner($group_id, bp_get_member_user_id())):
                        ?> <span class='unavailable' title='Already has a partner'>Unavailable</span>
                        <?php
                    // Do not show the partner option for the current logged in user
                    elseif (bp_get_member_user_id() != bp_loggedin_user_id()):
                        ?>
                        <a href='javascript:submitInnopdCreatePartnershipForm(<?php bp_group_member_id() ?>)' class='innovage_button'>Partner Up</a>
                    <?php endif; ?>
                </li>
            <?php endwhile; ?>
        </ul>
        <?php innovage_make_partnership_form($group_id); ?>

    <?php else: ?>

        <div id="message" class="info">
            <p>This group has no members.</p>
        </div>

    <?php endif; ?>
    <?php
}

/** /
 * Save a partner and team for two users
 * 
 * @param type $partner1
 * @param type $partner2
 * @param type $group_id
 * @return type
 */
function innovage_partnership_save_both($partner1, $partner2, $group_id) {
    $errorMessages = new WP_Error();

    if (empty($partner1) || empty($partner2)) {
        $errorMessages->add('required', __('Invalid partner id.'));
        return $errorMessages;
    }

    if ($partner1 == $partner2) {
        $errorMessages->add('required', __('You may not partner a user with themselves.'));
        return $errorMessages;
    }

    // Only save if the current user is logged in
    $user_id = get_current_user_id();
    if (!is_user_logged_in() || $user_id <= 0) {
        $errorMessages->add('required', __('Function only available to logged in users.'));
        return $errorMessages;
    }

    // Make sure partner id is from genuine user - if not end
    $partner2_real = get_userdata($partner2);
    if ($partner2_real == false) {
        $errorMessages->add('required', __('Invalid partner.'));
        return $errorMessages;
    }

    $partner1_real = get_userdata($partner1);
    if ($partner1_real == false) {
        $errorMessages->add('required', __('Invalid partner.'));
        return $errorMessages;
    }

    //Make sure partnership does not exist already
    if (is_duplicate_partnership($group_id, $partner1, $partner2)) {
        $errorMessages->add('required', __('This partnership already exists in the database.'));
        return $errorMessages;
    }

    //Make sure user does not already have a different partner
    if (innovage_partner_user_has_group_partner($group_id, $partner1)) {
        $errorMessages->add('required', __('One of the partners in the group is already partnered up.'));
        return $errorMessages;
    }

    //Make sure user does not already have a different partner
    if (innovage_partner_user_has_group_partner($group_id, $partner2)) {
        $errorMessages->add('required', __('One of the partners in the group is already partnered up.'));
        return $errorMessages;
    }

    $team_id = innovage_partner_add_team($group_id);
    innovage_partner_add_team_member($team_id, $partner1);
    innovage_partner_add_team_member($team_id, $partner2);
    return;
}

/** /
 * Wrapper function to save a partner and team for the current user
 *
 * @param type $partner_id
 * @param type $group_id
 * @return WP_Error
 */
function innovage_partnership_save($partner_id, $group_id) {
    $user_id = get_current_user_id();
    $errors = innovage_partnership_save_both($user_id, $partner_id, $group_id);
    return $errors;
}

/** /
 * Insert the team into the database
 *
 * @global type $wpdb
 * @param type $parent_group_id
 * @return type
 */
function innovage_partner_add_team($parent_group_id) {
    global $wpdb;
    $table_name = $wpdb->prefix . "innovage_teams";
    $current_user_id = get_current_user_id();

    $wpdb->insert($table_name, array('group_id' => $parent_group_id,
        'date_created' => current_time('mysql', true),
        'created_by' => $current_user_id));

    return $wpdb->insert_id;
}

/** /
 * Delete the team from the database
 *
 * @global type $wpdb
 * @param type $team_id
 */
function innovage_partner_delete_team($team_id) {
    global $wpdb;
    $table_name = $wpdb->prefix . "innovage_teams";

    $wpdb->delete($table_name, array('id' => $team_id));
}

/** /
 * For the moment automatically confirm new users
 * 
 * @global type $wpdb
 * @param type $team_id
 * @param type $user_id
 * @return type
 */
function innovage_partner_add_team_member($team_id, $user_id) {
    global $wpdb;
    $table_name = $wpdb->prefix . "innovage_team_users";
    $current_user_id = get_current_user_id();

    $wpdb->insert($table_name, array('team_id' => $team_id,
        'user_id' => $user_id,
        'is_confirmed' => 1,
        'date_created' => current_time('mysql', true),
        'created_by' => $current_user_id));
    return;
}

/** /
 * Delete the members of a partnership if they have the specified team id
 *
 * @global type $wpdb
 * @param type $team_id
 */
function innovage_partner_delete_team_members($team_id) {
    global $wpdb;
    $table_name = $wpdb->prefix . "innovage_team_users";

    $wpdb->delete($table_name, array('team_id' => $team_id));
}

/** /
 * Return true if user has a partner in a specified group, otherwise return false
 *
 * @param type $group_id
 * @param type $user_id
 * @return boolean
 */
function innovage_partner_user_has_group_partner($group_id, $user_id) {
    $result = innovage_partner_get_group_partner($user_id, $group_id);
    if (isset($result) and ! empty($result)) {
        return true;
    }
    return false;
}

/** /
 * Get the team_id, group_id and partner_id of a user in a group
 * This function will only work if each member only forms one partnership within a group
 *
 * @global type $wpdb
 * @param type $user_id
 * @param type $group_id
 * @return type
 */
function innovage_partner_get_group_partner($user_id, $group_id) {
    global $wpdb;

    $team_tblname = $wpdb->prefix . "innovage_teams";
    $team_users_tblname = $wpdb->prefix . "innovage_team_users";

    // Get the team_id for the the group and user
    $sql = $wpdb->prepare("SELECT $team_tblname.id "
            . " FROM $team_users_tblname "
            . " LEFT JOIN $team_tblname "
            . " ON $team_tblname.id= $team_users_tblname.team_id "
            . " WHERE user_id= %s"
            . " AND group_id=%s", $user_id, $group_id);

    $team_id = $wpdb->get_var($sql);

    // get the other member in that team
    $query = $wpdb->prepare("SELECT user_id, team_id, group_id "
            . " FROM $team_users_tblname "
            . " LEFT JOIN $team_tblname "
            . " ON $team_tblname.id= $team_users_tblname.team_id "
            . " WHERE user_id!= %s"
            . " AND team_id=%s", $user_id, $team_id);
    $result = $wpdb->get_row($query);
    return $result;
}

/** /
 * Get all the partnerships associated with a specific user
 * @global type $wpdb
 * @param type $user_id
 * @return type
 */
function innovage_partner_get_partnerships($user_id) {
    global $wpdb;

    $team_tblname = $wpdb->prefix . "innovage_teams";
    $team_users_tblname = $wpdb->prefix . "innovage_team_users";

    $sql = $wpdb->prepare("SELECT team_id FROM $team_users_tblname "
            . "WHERE user_id=%s", $user_id);

    // Get the partner teams the user is a member of
    $partner_teams = $wpdb->get_results($sql);

    $results = array();
    // For each partner-team get the users in that team(that aren't that user)
    // this is designed to only handle one partner per team at the moment
    // i.e. only a 1 to 1 relationship is available
    foreach ($partner_teams as $team) {

        $query = $wpdb->prepare("SELECT user_id, team_id, group_id "
                . " FROM $team_users_tblname "
                . " LEFT JOIN $team_tblname "
                . " ON $team_tblname.id= $team_users_tblname.team_id "
                . " WHERE user_id!= %s"
                . " AND team_id=%s", $user_id, $team->team_id);
        $data = $wpdb->get_results($query);

        $results[] = $data[0];
    }
    return $results;
}

/** /
 * Delete the team and the team members
 * @param type $team_id
 */
function innovage_partnership_delete($team_id) {
    innovage_partner_delete_create_deleted($team_id);
    innovage_partner_delete_team($team_id);
    innovage_partner_delete_team_members($team_id);
}

/** /
 * Get the partners based on the team id
 * @param type $team_id
 */
function innovage_partner_get_partners_by_team_id($team_id) {
    global $wpdb;
    $table_name = $wpdb->prefix . "innovage_team_users";

    $sql = $wpdb->prepare("SELECT user_id "
            . "FROM $table_name "
            . "WHERE team_id = %s", $team_id);
    $results = $wpdb->get_results($sql);

    return $results;
}

/** /
 * Create an entry in the database to allow us to track deleted partnership 
 * information
 * @param type $team_id 
 */
function innovage_partner_delete_create_deleted($team_id) {

    // Get the info to store to historical table before delete
    global $wpdb;
    $table_name = $wpdb->prefix . "innovage_teams";
    $sql = $wpdb->prepare("SELECT group_id, date_created, created_by "
            . "FROM $table_name "
            . "WHERE id = %s", $team_id);

    $team_info = $wpdb->get_row($sql);

    $group_id = $team_info->group_id;
    $created_date = $team_info->date_created;
    $created_by = $team_info->created_by;

    $partners = innovage_partner_get_partners_by_team_id($team_id);

    $user_id1 = $partners[0]->user_id;
    $user_id2 = $partners[1]->user_id;

    $table_name = $wpdb->prefix . "innovage_partners_deleted";
    $current_user_id = get_current_user_id();

    // save both partners to the partnership table
    $wpdb->insert($table_name, array('user_id' => $user_id1,
        'team_id' => $team_id,
        'group_id' => $group_id,
        'created_date' => $created_date,
        'created_by' => $created_by,
        'deleted_date' => current_time('mysql', true),
        'deleted_by' => $current_user_id));

    $wpdb->insert($table_name, array('user_id' => $user_id2,
        'team_id' => $team_id,
        'group_id' => $group_id,
        'created_date' => $created_date,
        'created_by' => $created_by,
        'deleted_date' => current_time('mysql', true),
        'deleted_by' => $current_user_id));
}

/** /
 * Check if the partnership already exists in the database
 * 
 * @global type $wpdb
 * @param type $group_id
 * @param type $user_id
 * @param type $partner_id
 * @return boolean
 */
function is_duplicate_partnership($group_id, $user_id, $partner_id) {
    global $wpdb;

    $team_tblname = $wpdb->prefix . "innovage_teams";
    $team_users_tblname = $wpdb->prefix . "innovage_team_users";

    $sql = $wpdb->prepare("SELECT a.team_id as t1, b.team_id as t2,a.group_id as g1, "
            . "b.group_id as g2,a.user_id as u1,b.user_id as u2 FROM "
            . "(SELECT team_id, group_id, user_id "
            . "FROM $team_tblname LEFT JOIN $team_users_tblname "
            . "ON $team_tblname.id = $team_users_tblname.team_id "
            . "WHERE group_id= %s"
            . "AND user_id=%s) AS a "
            . "JOIN( SELECT team_id,group_id,user_id "
            . "FROM $team_tblname "
            . "LEFT JOIN $team_users_tblname "
            . "ON $team_tblname.id = $team_users_tblname.team_id "
            . "WHERE group_id=%s "
            . "AND user_id=%s) AS b "
            . "ON a.team_id=b.team_id;", $group_id, $user_id, $group_id, $partner_id);

    // A bit of a dirty query to check for possible duplicate partnership
    $duplicate = $wpdb->get_row($sql);

    if (isset($duplicate->t1)) {
        return true;
    }
    return false;
}

/** /
 * Function to check if two users are partnered with one another
 * @global type $wpdb
 * @param type $user_id1
 * @param type $user_id2
 * @return boolean
 */
function innovage_partner_users_are_partners($user_id1, $user_id2) {
    // If $user_id1 equals $user_id2 then we have a logic error - return false
    if ($user_id1 === $user_id2) {
        return false;
    }

    global $wpdb;
    $table_name = $wpdb->prefix . "innovage_team_users";

    $sql1 = $wpdb->prepare("SELECT team_id "
            . "FROM $table_name "
            . "WHERE user_id= %s "
            . "AND is_confirmed=1", $user_id1);

    // Get all the partnerships that $user_id1 is a member of
    $teams_user1 = $wpdb->get_results($sql1);

    // Get all the partnerships that $user_id2 is a member of
    $sql2 = $wpdb->prepare("SELECT team_id "
            . "FROM $table_name "
            . "WHERE user_id= %s "
            . "AND is_confirmed=1", $user_id2);

    $teams_user2 = $wpdb->get_results($sql2);

    $teams1 = array();
    $teams2 = array();
    foreach ($teams_user1 as $team) {
        $teams1[] = $team->team_id;
    }
    foreach ($teams_user2 as $team) {
        $teams2[] = $team->team_id;
    }

    // If there is a match between the team_ids they are partners
    $matches = array_intersect($teams1, $teams2);

    // this is not working as expected - performing string conversion on associative array
    if (isset($matches) && !empty($matches)) {
        return true;
    }
    return false;
}

/** /
 * Get all of the team ids within a specified group
 * 
 * @global type $wpdb
 * @param type $group_id
 * @return type
 */
function innovage_partner_get_team_ids_by_group($group_id) {
    global $wpdb;
    $table_name = $wpdb->prefix . "innovage_teams";
    $sql = $wpdb->prepare("SELECT id "
            . "FROM $table_name "
            . "WHERE group_id = %s", $group_id);
    $results = $wpdb->get_results($sql);
    return $results;
}

/** /
 * Get all of the partners per team within a group
 * 
 * @global type $wpdb
 * @param type $group_id
 * @return array
 */
function innovage_partner_get_partners_by_group($group_id) {
    $teams = innovage_partner_get_team_ids_by_group($group_id);

    $partnerships = array();
    if (isset($teams) && !empty($teams)) {
        foreach ($teams as $team) {
            $results = innovage_partner_get_partners_by_team_id($team->id);
            $team_partners = array();
            foreach ($results as $result) {
                array_push($team_partners, $result->user_id);
            }
            $partnerships[$team->id] = $team_partners;
        }
    }
    return $partnerships;
}

add_action('delete_user', 'innovage_partner_delete_user_partnerships');

/** /
 * When a user is deleted make sure to delete their associated partnerships
 * 
 * @param type $user_id
 */
function innovage_partner_delete_user_partnerships($user_id) {
    // get all partnerships associalted with the user
    $partner_team_info = innovage_partner_get_partnerships($user_id);
    if (isset($partner_team_info) && !empty($partner_team_info)) {
        for ($i = 0; $i < count($partner_team_info); ++$i) {
            $team_id = $partner_team_info[$i]->team_id;

            // delete the team and its members
            innovage_partnership_delete($team_id);
        }
    }
}

add_action('groups_remove_member', 'innovage_partner_remove_member', 15, 2);
add_action('groups_ban_member', 'innovage_partner_ban_member', 10, 2);

/** /
 * Gets called when a member is banned and kicked out by an admin.
 * Deletes any partnerships they have created within the group.
 *
 * @param type $group_id
 * @param type $user_id
 */
function innovage_partner_ban_member($group_id, $user_id) {
    // A banned member is still a member of the group, however 
    // if they have a partner we want to free up that partner
    // for someone else
    innovage_partner_leave_group($group_id, $user_id);
}

/** /
 * Gets called when a user is removed from a group by a group admin. 
 * Deletes any partnership they have created within the group.
 * 
 * @param type $group_id
 * @param type $user_id
 */
function innovage_partner_remove_member($group_id, $user_id) {
    innovage_partner_leave_group($group_id, $user_id);
}

add_action('groups_leave_group', 'innovage_partner_leave_group', 10, 2);

/** /
 * Gets called when a user leaves a group under their own steam
 * Deletes any partnership they have created within the group.
 *
 * @global type $wpdb
 * @param type $group_id
 * @param type $user_id
 */
function innovage_partner_leave_group($group_id, $user_id) {
    global $wpdb;

    $team_tblname = $wpdb->prefix . "innovage_teams";
    $team_users_tblname = $wpdb->prefix . "innovage_team_users";

    //Get the team id using the group id and the user_id
    $sql = $wpdb->prepare("SELECT team_id FROM $team_tblname "
            . "LEFT JOIN $team_users_tblname "
            . "ON $team_tblname.id = $team_users_tblname.team_id "
            . "WHERE group_id=%s "
            . "AND user_id=%s", $group_id, $user_id);

    // Just incase the specs go out the window and we have multiple partnerships
    // check for multiple
    $team_ids = $wpdb->get_results($sql);

    foreach ($team_ids as $team) {
        // delete the team and its members
        innovage_partnership_delete($team->team_id);
    }
}

/** /
 * Get steps for members of a collaborative dyad challenge group betwen two set dates
 * 
 * @global type $wpdb
 * @param type $group_id
 * @param type $start_date
 * @param type $end_date
 * @return type
 */
function innovage_partner_collaborative_dyad_steps($group_id, $start_date, $end_date) {
    global $wpdb;
    $table1_name = $wpdb->prefix . "innovage_teams";
    $table2_name = $wpdb->prefix . "innovage_team_users";

    $results = null;

    $sql = $wpdb->prepare("SELECT id "
            . "FROM $table1_name "
            . "WHERE group_id=%s ", $group_id);
    $team_ids = $wpdb->get_results($sql);

    foreach ($team_ids as $team_id) {
        $sql2 = $wpdb->prepare("SELECT user_id "
                . "FROM $table2_name "
                . "WHERE team_id=%s"
                . "AND is_confirmed=1", $team_id->id);
        $members = $wpdb->get_results($sql2);

        $steps = array();
        foreach ($members as $member) {
            $user_step_cnt = innovage_pedometer_get_user_steps($member->user_id, $start_date, $end_date);
            $results[$team_id->id]['members'][$member->user_id] = $user_step_cnt;
            array_push($steps, $user_step_cnt);
        }

        $result = innovage_partner_collaborative_dyad_steps_pair($steps[0], $steps[1]);
        $results[$team_id->id]['pair_steps'] = $result;
    }

    return $results;
}

/** /
 * Get steps for members of a competitive dyad challenge group betwen two set dates
 *
 * @global type $wpdb
 * @param type $group_id
 * @param type $start_date
 * @param type $end_date
 * @return type
 */
function innovage_partner_basic_dyad_steps($group_id, $start_date, $end_date) {
    global $wpdb;
    $table1_name = $wpdb->prefix . "innovage_teams";
    $table2_name = $wpdb->prefix . "innovage_team_users";

    $results = null;

    $sql = $wpdb->prepare("SELECT id "
            . "FROM $table1_name "
            . "WHERE group_id=%s ", $group_id);
    $team_ids = $wpdb->get_results($sql);

    foreach ($team_ids as $team_id) {
        $sql2 = $wpdb->prepare("SELECT user_id "
                . "FROM $table2_name "
                . "WHERE team_id=%s"
                . "AND is_confirmed=1", $team_id->id);
        $members = $wpdb->get_results($sql2);

        $steps = array();
        foreach ($members as $member) {
            $user_step_cnt = innovage_pedometer_get_user_steps($member->user_id, $start_date, $end_date);
            $results[$team_id->id]['members'][$member->user_id] = $user_step_cnt;
            array_push($steps, $user_step_cnt);
        }

        $mean = innovage_partner_basic_dyad_steps_pair($steps[0], $steps[1]);
        $results[$team_id->id]['pair_steps'] = $mean;
    }

    return $results;
}

/** /
 * Calculate the dyad step calculation based on the equation
 * A + B + sqrt(A*B)
 *
 * @param type $member1_steps
 * @param type $member2_steps
 * @return type
 */
function innovage_partner_basic_dyad_steps_pair($member1_steps, $member2_steps) {
    $sum = $member1_steps + $member2_steps;
    $mul = $member1_steps * $member2_steps;

    // A + B + sqrt(A*B)
    $total = $sum + sqrt($mul);
    return round($total);
}

/** /
 * Calculate the pair steps for collaborative dyad challenge pair. Andy insists that
 * this will be different than the calculation for the competitive dyad challange but 
 * has as of yet failed to say what would be different about it... so falling back
 * on the basic_dyad calculation for the moment
 * 
 * @param type $member1_steps
 * @param type $member2_steps
 */
function innovage_partner_collaborative_dyad_steps_pair($member1_steps, $member2_steps) {
    return innovage_partner_basic_dyad_steps_pair($member1_steps, $member2_steps);
}

/** /
 * Call function in innovage_routes plugin that will return the map visualisation
 * 
 * @param type $route_id
 * @param type $percent_complete
 * @return string
 */
function innovage_partner_get_visualisation($route_id, $percent_complete) {
    if (function_exists('innovage_routes_get_visualisation')) {
        innovage_routes_get_visualisation($route_id, $percent_complete);
    }
}

/** /
 * Calculate the dyad percentage complete for a specific group pair
 *
 * @param type $group_id
 * @param type $user_id
 * @param type $partner_id
 * @return type
 */
function innovage_partner_get_dyad_percentage($group_id, $user_id, $partner_id) {
    $group = groups_get_group(array('group_id' => $group_id));
    $route_id = groups_get_groupmeta($group_id, 'challenge-type');
    $total_steps = innovage_routes_get_goal_amount($route_id);
    $end_date = groups_get_groupmeta($group_id, 'challenge-end-date');
    $start_date = strtok($group->date_created, " ");
    $member1_steps = innovage_pedometer_get_user_steps($user_id, $start_date, $end_date);
    $member2_steps = innovage_pedometer_get_user_steps($partner_id, $start_date, $end_date);

    $challenge_type = groups_get_groupmeta($group_id, 'challenge-approach');

    // If challenge is competitive dyad type
    if ($challenge_type == 1) {
        $dyad_total = innovage_partner_basic_dyad_steps_pair($member1_steps, $member2_steps);
    }
    // If challenge is collaborative dyad challenge type
    else if ($challenge_type == 2) {
        $dyad_total = innovage_partner_collaborative_dyad_steps_pair($member1_steps, $member2_steps);
    }

    $percentage_complete = intval(($dyad_total / $total_steps) * 100);
    return $percentage_complete;
}

add_action('groups_before_delete_group', 'innovage_pedometer_delete_group_partnerships');

/** /
 * When a group is deleted delete the associated partnerships
 * @param type $group_id
 */
function innovage_pedometer_delete_group_partnerships($group_id) {
    if (bp_group_has_members("group_id=" . $group_id . '&exclude_admins_mods=0')) {
        while (bp_group_members()) : bp_group_the_member();
            // for each member of group remove their partnerships
            innovage_partner_leave_group($group_id, bp_get_group_member_id());
        endwhile;
    }
}

/** /
 * Return steps walked by the combined dyads (which is not the same as adding 
 * the individual steps together).
 * i.e. the combined steps walked for a collaborative challenge
 * 
 * @param type $group_id
 * @param type $start_date
 * @param type $end_date
 * @return type
 */
function innovage_partner_collaborative_dyad_steps_combined($group_id, $start_date, $end_date) {
    $collaborative_walked = 0;
    $collaborative_pair_results = innovage_partner_collaborative_dyad_steps($group_id, $start_date, $end_date);
    foreach ($collaborative_pair_results as $team_data) {
        $collaborative_walked +=$team_data['pair_steps'];
    }

    return $collaborative_walked;
}
