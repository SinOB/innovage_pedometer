<?php

/* Add a menu option to link to the steps section of the logged in users profile */
add_filter('wp_nav_menu_items', 'innovage_pedometer_main_menu_profile_link');

function innovage_pedometer_main_menu_profile_link($menu) {
    // If user not logged in return default menu
    if (!is_user_logged_in()) {
        return $menu;
    } else {
        // Add profile link to the end of the menu
        $profilelink = '<li><a href="' . bp_loggedin_user_domain('/') . 'steps/">' . __('Steps') . '</a></li>';
    }
    $menu = $menu . $profilelink;
    return $menu;
}

function innovage_pedometer_member_steps_setup_nav() {
    global $bp;

    $parent_slug = 'steps';
    bp_core_new_nav_item(array(
        'name' => __('Steps'),
        'slug' => $parent_slug,
        'screen_function' => 'innovage_pedometer_member_steps_screen',
        'position' => 40,
        'default_subnav_slug' => 'test_sub'));

    function innovage_pedometer_bp_member_steps_show_screen_title() {
        
    }

    function innovage_pedometer_bp_member_steps_show_screen_content() {
        $displayed_user_id = bp_displayed_user_id();
        $logged_in_user_id = get_current_user_id();

        //If the user is logged in
        if (is_user_logged_in()) {
            $is_partner = 0;
            // If the displayed user is a partner of the logged in user
            if (function_exists('innovage_partner_users_are_partners')) {
                if (innovage_partner_users_are_partners($logged_in_user_id, $displayed_user_id)) {
                    $is_partner = 1;
                }
            }

            // If the displayed user is the loggedin user
            // or the displayed user is the current users partner
            // or the current user is an admin
            // or the current user is an editor
            if ($displayed_user_id === $logged_in_user_id || $is_partner == 1 || current_user_can('edit_pages')) {
                //display the steps form
                $steps_form = innovage_pedometer_step_form($is_partner);
                echo $steps_form;
            }
        }

        // Display a graph for each challenge the user is a member of
        if (bp_has_groups(array('user_id' => $displayed_user_id))) :
            while (bp_groups()) : bp_the_group();
                $group_id = bp_get_group_id();
                innovage_pedometer_graph_member($displayed_user_id, $group_id);
            endwhile;
        endif;
    }
}

function innovage_pedometer_member_steps_screen() {
    add_action('bp_template_title', 'innovage_pedometer_bp_member_steps_show_screen_title');
    add_action('bp_template_content', 'innovage_pedometer_bp_member_steps_show_screen_content');
    bp_core_load_template(apply_filters('bp_core_template_plugin', 'members/single/plugins'));
}

add_action('bp_setup_nav', 'innovage_pedometer_member_steps_setup_nav');
