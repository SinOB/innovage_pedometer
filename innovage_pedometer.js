
function submitInnopdDeleteForm(step_date)
{
    if (!confirm('Are you sure you want to delete your steps for this date?'))
    {
        return;
    }
    jQuery('#innopd_step_date').val(step_date);
    jQuery('#innopd_delete_step').submit();
}

jQuery(function() {
    jQuery('.progressbar_individual').each(function() {
        var value = parseInt(jQuery(this).text());
        jQuery(this).empty().progressbar({value: value});
    });
});


function submitInnopdCreatePartnershipForm(partner_user_id)
{
    if (!confirm('Are you sure you want to partner with this user?'))
    {
        return;
    }
    jQuery('#innopt_partner_id').val(partner_user_id);
    jQuery('#innopt_partner_action').val('add');
    jQuery('#innopt_make_partnership').submit();
}

function submitInnopdDeletePartnershipForm(team_id)
{
    if (!confirm('Are you sure you want to delete this partnership?'))
    {
        return;
    }
    jQuery('#innopt_team_id').val(team_id);
    jQuery('#innopt_partner_action').val('delete');
    jQuery('#innopt_make_partnership').submit();
}

jQuery(function() {
    jQuery('.progressbar').each(function() {
        var value = parseInt(jQuery(this).text());
        jQuery(this).empty().progressbar({value: value});
    });
});

